#!/bin/bash 

# 结束tomcat
cd /mnt/tomcat8/bin/
./shutdown.sh
# 结束 root 用户 执行的所有 java 进程的命令（除了本身grep的这个之外）,免得没能成功结束tomcat
# 因为要重启，所以这个也不用结束了
# ps -ef | grep java | grep root | grep -v grep |awk '{print $2}' | xargs --no-run-if-empty kill -9


############支持包##############

# wm
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wm-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/version/v6.4/wm-3.10.jar

# wangmarket
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/version/v6.4/wangmarket-6.4.jar

# http
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/http-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/version/v6.4/http-1.2.jar

# xnx3-util
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xnx3-util-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/version/v6.4/xnx3-util-1.16.jar


# moreDomain
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.moreDomain-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.moreDomain-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/moreDomain/wangmarket.plugin.moreDomain-1.8.jar
fi

# kefu
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.kefu-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.kefu-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/kefu/wangmarket.plugin.kefu-1.2.jar
fi

# phoneCreateSite
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.phoneCreateSite-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.phoneCreateSite-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/phoneCreateSite/wangmarket.plugin.phoneCreateSite-1.5.jar
fi

# htmlSeparate 的sftp需要的jar
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.htmlSeparate-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/jsch-*.jar
	wget http://down.zvo.cn/fileupload/storage/sftp/jsch-0.1.55.jar
fi


##########################


############## 数据库方面更新 ###############
wget http://down.zvo.cn/properties/properties-1.0.1.jar -O ~/properties.jar
# 数据库IP
database_ip=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.ip`
# 数据库名
database_name=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.name`
# 数据库登录用户名
database_username=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.username`
# 数据库登录密码
database_password=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.password`
####### 数据库执行相关命令
# site.name 增加长度到120（实际长度为不超过60个字符）
mysql -h $database_ip -u$database_username -p$database_password $database_name -e "alter table site modify column name char(120);"
# site_column.ico 长度增加到250
mysql -h $database_ip -u$database_username -p$database_password $database_name -e "ALTER TABLE site_column MODIFY COLUMN icon char(250);"


#########

# 重启tomcat
wget https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms/raw/master/shell/restart_tomcat.sh -O /mnt/tomcat8/bin/restart_tomcat.sh && chmod -R 777 /mnt/tomcat8/bin/restart_tomcat.sh && nohup /mnt/tomcat8/bin/restart_tomcat.sh &
