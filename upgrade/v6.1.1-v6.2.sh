#!/bin/bash 

# 结束tomcat
cd /mnt/tomcat8/bin/
./shutdown.sh
# 结束 root 用户 执行的所有 java 进程的命令（除了本身grep的这个之外）,免得没能成功结束tomcat
# 因为要重启，所以这个也不用结束了
# ps -ef | grep java | grep root | grep -v grep |awk '{print $2}' | xargs --no-run-if-empty kill -9


############支持包##############

# wangmarket
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket-*.jar
wget http://down.zvo.cn/wangmarket/version/v6.2/wangmarket-6.2.jar

# wm
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wm-*.jar
wget http://down.zvo.cn/wangmarket/version/v6.2/wm-3.3.jar

# templateCenter
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.templateCenter-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.templateCenter-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/templateCenter/wangmarket.plugin.templateCenter-1.5.jar
fi

# siteapi
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteapi-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteapi-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/siteapi/wangmarket.plugin.siteapi-2.1.jar
fi

# HtmlVisualEditor 可视化html编辑
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.HtmlVisualEditor-*.jar
wget http://down.zvo.cn/wangmarket/plugin/HtmlVisualEditor/wangmarket.plugin.HtmlVisualEditor-3.0.1.jar

# upgrade
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.upgrade-*.jar
wget http://down.zvo.cn/wangmarket/plugin/upgrade/wangmarket.plugin.upgrade-1.2.4.jar
# 增加相关class文件
mkdir /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/
mkdir /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/
mkdir /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/upgrade/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/upgrade/Update.class
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/upgrade/
wget http://down.zvo.cn/wangmarket/plugin/upgrade/wangmarket_UpdateClass/Update.class?v=1.2.2 -O /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/upgrade/Update.class

# 客服插件的客服模板安装
mkdir /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/static/plugin/kefu/template/ -p
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/static/plugin/kefu/template/*
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/static/plugin/kefu/template/
wget http://down.zvo.cn/wangmarket/plugin/kefu/template_v1.0.zip -O template.zip
# yum -y install unzip
unzip template.zip
rm -rf template.zip

# moreDomain
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.moreDomain-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.moreDomain-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/moreDomain/wangmarket.plugin.moreDomain-1.7.1.jar
fi

# seo
wget http://down.zvo.cn/properties/properties-1.0.1.jar -O ~/properties.jar
authorize=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get authorize`
if [ -n "$authorize" ]; then
	# 已配置授权码
    cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.seo-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/seo/wangmarket.plugin.seo-3.0.2.jar
fi

# htmlSeparate
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.htmlSeparate-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/htmlSeparate/wangmarket.plugin.htmlSeparate-1.13.jar



##########################
# 重启tomcat
wget https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms/raw/master/shell/restart_tomcat.sh -O /mnt/tomcat8/bin/restart_tomcat.sh && chmod -R 777 /mnt/tomcat8/bin/restart_tomcat.sh && nohup /mnt/tomcat8/bin/restart_tomcat.sh &
