#!/bin/bash 

# 结束tomcat
cd /mnt/tomcat8/bin/
./shutdown.sh
# 结束 root 用户 执行的所有 java 进程的命令（除了本身grep的这个之外）,免得没能成功结束tomcat
# 因为要重启，所以这个也不用结束了
# ps -ef | grep java | grep root | grep -v grep |awk '{print $2}' | xargs --no-run-if-empty kill -9


############支持包##############

# wangmarket
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/version/v6.3/wangmarket-6.3.jar

# wm
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wm-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/version/v6.3/wm-3.6.jar


# siteapi
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteapi-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteapi-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/siteapi/wangmarket.plugin.siteapi-2.2.jar
fi

# htmlSeparate
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.htmlSeparate-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.htmlSeparate-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/htmlSeparate/wangmarket.plugin.htmlSeparate-1.14.jar
fi

# ueditor
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/fileupload-framework-ueditor-*.jar
wget http://down.zvo.cn/wangmarket/version/v6.2/fileupload-framework-ueditor-1.2.jar

# fileupload-springboot
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/fileupload-framework-springboot-*.jar
wget http://down.zvo.cn/wangmarket/version/v6.3/fileupload-framework-springboot-1.3.1.jar

# HtmlVisualEditor
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.HtmlVisualEditor-*.jar
wget http://down.zvo.cn/wangmarket/plugin/HtmlVisualEditor/wangmarket.plugin.HtmlVisualEditor-3.0.2.jar

# moreDomain
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.moreDomain-*.jar
wget http://down.zvo.cn/wangmarket/plugin/moreDomain/wangmarket.plugin.moreDomain-1.8.jar


##########################


############## 数据库方面更新 ###############
wget http://down.zvo.cn/properties/properties-1.0.1.jar -O ~/properties.jar
# 数据库IP
database_ip=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.ip`
# 数据库名
database_name=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.name`
# 数据库登录用户名
database_username=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.username`
# 数据库登录密码
database_password=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.password`
####### 数据库执行相关命令
# site.name 增加长度到120（实际长度为不超过60个字符）
mysql -h $database_ip -u$database_username -p$database_password $database_name -e "alter table site modify column name char(120);"
# site_column.ico 长度增加到250
mysql -h $database_ip -u$database_username -p$database_password $database_name -e "ALTER TABLE site_column MODIFY COLUMN icon char(250);"


#########

# 重启tomcat
wget https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms/raw/master/shell/restart_tomcat.sh -O /mnt/tomcat8/bin/restart_tomcat.sh && chmod -R 777 /mnt/tomcat8/bin/restart_tomcat.sh && nohup /mnt/tomcat8/bin/restart_tomcat.sh &
