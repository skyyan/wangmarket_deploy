#!/bin/bash 

# 结束tomcat
cd /mnt/tomcat8/bin/
./shutdown.sh
# 结束 root 用户 执行的所有 java 进程的命令（除了本身grep的这个之外）,免得没能成功结束tomcat
# 因为要重启，所以这个也不用结束了
# ps -ef | grep java | grep root | grep -v grep |awk '{print $2}' | xargs --no-run-if-empty kill -9


############支持包##############

# moreDomain
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.moreDomain-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.moreDomain-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/moreDomain/wangmarket.plugin.moreDomain-1.8.jar
fi

# kefu
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.kefu-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.kefu-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/kefu/wangmarket.plugin.kefu-1.2.jar
fi

# phoneCreateSite
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.phoneCreateSite-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.phoneCreateSite-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/phoneCreateSite/wangmarket.plugin.phoneCreateSite-1.6.jar
fi

# htmlSeparate 的sftp需要的jar
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.htmlSeparate-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/jsch-*.jar
	wget http://down.zvo.cn/fileupload/storage/sftp/jsch-0.1.55.jar
fi

# fileupload-core
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/fileupload-core-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/fileupload-core-*.jar
	wget http://down.zvo.cn/fileupload/core/fileupload-core-2.0.jar
fi

# fileupload-framework-ueditor
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/fileupload-framework-ueditor-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/fileupload-framework-ueditor-*.jar
	wget http://down.zvo.cn/fileupload/framework/ueditor/fileupload-framework-ueditor-1.3.jar
fi

# log-core
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/log-core-*.jar 2>&1;then
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/log-core-*.jar
	wget http://down.zvo.cn/wangmarket/version/v6.5/log-core-1.2.jar
fi

# wangmarket
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket-*.jar
wget http://down.zvo.cn/wangmarket/version/v6.5/wangmarket-6.5.jar

# wm
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wm-*.jar
wget http://down.zvo.cn/wangmarket/version/v6.5/wm-3.14.jar

# 删除 wangmarket.plugin.rootTxtUpload-1.1.jar 因为这个会跟v6.5的 /*.txt 冲突
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.rootTxtUpload-*.jar


############## application.properties方面更新 ###############
# 备份配置文件
mkdir -p /mnt/backups/upgrade/v6.5/
cp -r /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties /mnt/backups/upgrade/v6.5/application.properties

# 下载properties.jar
wget http://down.zvo.cn/properties/properties-1.0.1.jar -O ~/properties.jar
# 设置 fileupload size
java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -set fileupload.maxSize=30MB
# 开启mysql数据库自动备份
java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -set wm.databaseAutoBackups.use=true


############## 数据库方面更新 ###############
# 数据库IP
database_ip=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.ip`
# 数据库名
database_name=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.name`
# 数据库登录用户名
database_username=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.username`
# 数据库登录密码
database_password=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.password`
####### 数据库执行相关命令
# SITE_DESCRIPTION、SITE_KEYWORDS 赋予空字符
mysql -h $database_ip -u$database_username -p$database_password $database_name -e "UPDATE system SET value='' WHERE name = 'SITE_DESCRIPTION' AND value = '管雷鸣'"
mysql -h $database_ip -u$database_username -p$database_password $database_name -e "UPDATE system SET value='' WHERE name = 'SITE_KEYWORDS' AND value = '网市场云建站系统'"

##########################

# 重启tomcat
wget https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms/raw/master/shell/restart_tomcat.sh -O /mnt/tomcat8/bin/restart_tomcat.sh && chmod -R 777 /mnt/tomcat8/bin/restart_tomcat.sh && nohup /mnt/tomcat8/bin/restart_tomcat.sh &
